Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  root 'home#index'
  namespace :api, { format: "json" } do
    namespace :v1 do
      resources :tasks, :only => [:index, :create, :update, :destroy]
    end
  end
end
