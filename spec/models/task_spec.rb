require 'rails_helper'

RSpec.describe Todo, type: :model do
  describe "validates presence" do
    context "bodyを指定しているとき" do
      let(:task) { create(:task) }
      it "レコードが作成される" do
        expect(task).to be_valid
      end
    end

    context "bodyを指定していないとき" do
      let(:task) { build(:task, body: nil) }
      it "エラーになる" do
        task.valid?
        expect(task.errors.messages[:body]).to include "can't be blank"
      end
    end
  end
end