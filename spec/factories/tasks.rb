FactoryBot.define do
  factory :task do
    body { "MyString" }
    done { false }
  end
end
