class TaskSerializer < ActiveModel::Serializer
  attributes :id, :body, :done
end
