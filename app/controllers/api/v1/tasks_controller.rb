class Api::V1::TasksController < ApplicationController
  before_action :set_task, only: [:update, :destroy]

  def index
    @task = Task.all.order("created_at DESC")
    render json: @task
  end

  def create
    @task = Task.create!(task_params)
    #作成したデータはリストに追加していくためデータを返す
    render json: @task
  end

  def update
    @task.update!(task_params)
  end

  def destroy
    @task.destroy!
  end

  private
    def set_task
        @task = Task.find(params[:id])
    end

    def task_params
        params.require(:task).permit(:body, :done)
    end
end
